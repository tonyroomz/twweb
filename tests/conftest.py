import pytest
import sys
import os

from flask import Flask

# make helpers importable
sys.path.append(os.path.join(os.path.dirname(__file__), 'helpers'))

@pytest.fixture
def app():
    app = Flask(__name__)
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    app.config['SECRET_KEY'] = 'asdf'
    return app

@pytest.fixture
def client(app):
    client = app.test_client()
    with app.app_context():
        with app.test_request_context():
            yield client
